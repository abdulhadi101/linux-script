#! /bin/bash

bosskey="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYNYf60Tt+t2P8xbwX9j2ODh6f+wupgAbH3uTYDIBgsiVLsgQFjdWaLvBWv2JdqQfR+OfsMERvbbTT3uHBR2FdEvXZ15s6DPERhTmBprRzKMReGj6OpgO/c5iy6CGkAncNmRQ6tL9WIu7G6L7gWldvMavrhveJ8JXcvjN++c+e9C82TkQ0mYsrRX29FRfqeqyQ/yTheYiTuudy8iMwxUVWgObRBm2CDPTVfFSLP9eC5pI9WXQlG7/hReocL4BzEjHNBcGDVtLs9GQauFRLQBHKlX+dTilRBSwWP2JS9C74feq5Mk32pkP4cJIFAsHuQ6otVwftMwdTzET1GbiGu6C9"



echo "Updating Server"
sudo apt update

# Adding SSH public key to the authorized_keys file

sudo sh -c 'echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYNYf60Tt+t2P8xbwX9j2ODh6f+wupgAbH3uTYDIBgsiVLsgQFjdWaLvBWv2JdqQfR+OfsMERvbbTT3uHBR2FdEvXZ15s6DPERhTmBprRzKMReGj6OpgO/c5iy6CGkAncNmRQ6tL9WIu7G6L7gWldvMavrhveJ8JXcvjN++c+e9C82TkQ0mYsrRX29FRfqeqyQ/yTheYiTuudy8iMwxUVWgObRBm2CDPTVfFSLP9eC5pI9WXQlG7/hReocL4BzEjHNBcGDVtLs9GQauFRLQBHKlX+dTilRBSwWP2JS9C74feq5Mk32pkP4cJIFAsHuQ6otVwftMwdTzET1GbiGu6C9" >>  /root/.ssh/authorized_keys'

#Installing Apache webserver
echo "Installing Apache2"
sudo apt -y install apache2

#Installing Mysql server
sudo apt -y install mysql-server

#Installing PHP and some of its dependencies
sudo apt -y install php libapache2-mod-php

sudo apt -y install php-xml

#Creating and updating Apache configuration Files

touch dir.conf

echo "<IfModule mod_dir.c>
    DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
</IfModule>" > dir.conf
#Copying the created config file the desired location and making a backup 
#of the existing config file before overwritting (dir.conf~)
sudo cp -b dir.conf /etc/apache2/mods-enabled/dir.conf



#Testing PHP Processing on the Web Server
touch info.php

echo "<?php 
phpinfo(); 
?>" > info.php
sudo cp info.php /var/www/html/

#Installing Git in case its not already installed
sudo apt -y install git

#Cloning the phpsysinfo repository
git clone https://github.com/phpsysinfo/phpsysinfo.git

#Moving the phpsysinfo app the webserver Document_Root
sudo mv phpsysinfo /var/www/html/phpsysinfo

#Setting up the app by Creating the phpsysinfo.ini file
sudo mv /var/www/html/phpsysinfo/phpsysinfo.ini.new /var/www/html/phpsysinfo/phpsysinfo.ini

#Restarting Apache webserver
sudo systemctl restart apache2 

#System clean Up by Removing redundant files
sudo rm info.php
sudo rm dir.conf
sudo rm -rf phpsysinfo


