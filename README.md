# linux-script

This is repo contains both a script and an ansible playbook to do the following;
- Update the server
- Install git, Apache, MySQL, PHP and other dependencies
- Set up UFW firewall to allow http and https connection
- Add a SSH key to the server authorized keys
- Create a phpinfo page
- Set up Mysql
- Install phpsysinfo app on the server
- Adjusting configuration files
